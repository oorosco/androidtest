package patrykpoborca.io.androidtest.imageselection.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;
import patrykpoborca.io.androidtest.R;
import patrykpoborca.io.androidtest.model.PugModel;

/**
 * Created by patrykpoborca on 4/26/16.
 */
public class PugViewHolder extends RecyclerView.ViewHolder {

    @NonNull
    @Bind(R.id.pug_image)
    public ImageView pugImage;

    public PugViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void onBind(PugModel pugModel) {
        Glide.with(pugImage.getContext())
                .load(pugModel.getImageUrl())
                .crossFade()
                .fitCenter()
                .into(pugImage);

        pugImage.setOnLongClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) itemView.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("copied", pugModel.getImageUrl());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(pugImage.getContext(), "Copied!", Toast.LENGTH_SHORT).show();
            return true;
        });
    }

    public static PugViewHolder inflate(@NonNull ViewGroup parent) {
        return new PugViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.vh_pug, parent, false));
    }

}
