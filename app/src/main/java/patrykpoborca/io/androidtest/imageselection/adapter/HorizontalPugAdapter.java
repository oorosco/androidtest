package patrykpoborca.io.androidtest.imageselection.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import patrykpoborca.io.androidtest.model.PugModel;
import rx.Observable;

/**
 * Created by patrykpoborca on 4/26/16.
 */
public class HorizontalPugAdapter extends PugAdapter {

    private final int screenWidth;

    public HorizontalPugAdapter(Observable<List<PugModel>> pugStream, List<PugModel> currentPugs, OnPugTouched onPugTouched, int screenWidth) {
        super(pugStream, currentPugs, onPugTouched);
        this.screenWidth = screenWidth;
    }

    @Override
    public PugViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PugViewHolder v = PugViewHolder.inflate(parent);
        v.itemView.setLayoutParams(new RecyclerView.LayoutParams(screenWidth, screenWidth));
        return v;
    }
}
