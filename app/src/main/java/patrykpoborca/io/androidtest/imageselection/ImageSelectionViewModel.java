package patrykpoborca.io.androidtest.imageselection;

import java.util.ArrayList;
import java.util.List;

import patrykpoborca.io.androidtest.base.BaseViewModel;
import patrykpoborca.io.androidtest.model.PugModel;
import patrykpoborca.io.androidtest.network.PugService;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by patrykpoborca on 4/26/16.
 */
public class ImageSelectionViewModel extends BaseViewModel {

    private BehaviorSubject<List<PugModel>> pugStream = BehaviorSubject.create();
    private boolean loading;
    private PugService pugService;
    private List<PugModel> currentPugs = new ArrayList<>(20);

    public Observable<List<PugModel>> getPugStream() {
        return pugStream;
    }

    @Override
    public void onAttach() {
        super.onAttach();
        pugService = pugService == null ? PugService.Factory.instantiate(getRetrofit()) : pugService;
        if (currentPugs.size() == 0) {
            requestMorePugs();
        }
    }

    public boolean isLoading() {
        return loading;
    }

    public Observable<List<PugModel>> requestMorePugs() {
        if (loading) {
            return Observable.never();
        }
        loading = true;
        Observable<List<PugModel>> obs = pugService.getPugs(50)
                .doOnNext(p -> loading = false)
//        Observable.just(new Gson().fromJson("{\"pugs\":[\"http://27.media.tumblr.com/tumblr_lj4x7uXySA1qcbrufo1_500.jpg\",\"http://24.media.tumblr.com/tumblr_lteechLckg1qb08qmo1_500.jpg\",\"http://25.media.tumblr.com/tumblr_ljguyvQfz81qzy9e6o1_500.jpg\",\"http://25.media.tumblr.com/tumblr_mcmthlSKeN1r0wqrdo1_500.gif\",\"http://27.media.tumblr.com/tumblr_ls6tpnEwe71r3ip8io1_500.jpg\",\"http://25.media.tumblr.com/tumblr_mbfl4luabM1qb08qmo1_500.jpg\",\"http://37.media.tumblr.com/tumblr_mbg13ySRTZ1qbye9vo1_500.png\",\"http://30.media.tumblr.com/tumblr_lisw5dD4Pu1qbbpjfo1_400.jpg\",\"http://31.media.tumblr.com/tumblr_mbs9uw4Uoy1qaa50yo1_500.jpg\",\"http://28.media.tumblr.com/tumblr_lj0eomAZZ91qb08qmo1_500.jpg\",\"http://24.media.tumblr.com/tumblr_ljjmvou3oc1qfz5nco1_500.jpg\",\"http://25.media.tumblr.com/tumblr_lk7v8zCcIn1qaa50yo1_500.jpg\",\"http://33.media.tumblr.com/tumblr_mcei0s9zPy1qzxd5lo1_500.jpg\",\"http://29.media.tumblr.com/tumblr_locarpP8Sd1qakea9o1_400.jpg\",\"http://33.media.tumblr.com/2e5eb3171fc933f19786610345879fcf/tumblr_mozdou8yEC1qb08qmo1_500.jpg\",\"http://24.media.tumblr.com/tumblr_lsvcpxVBgd1qzgqodo1_500.jpg\",\"http://25.media.tumblr.com/tumblr_mblnsnvibw1r3pfomo1_500.jpg\",\"http://24.media.tumblr.com/tumblr_lk9flykRUm1qbiuveo1_500.jpg\",\"http://25.media.tumblr.com/tumblr_li5uwk1P0W1qepvs6o1_400.jpg\",\"http://25.media.tumblr.com/tumblr_ls1kpxCTQz1qlw5fmo1_500.jpg\",\"http://36.media.tumblr.com/tumblr_mbg13ySRTZ1qbye9vo1_500.png\",\"http://26.media.tumblr.com/tumblr_lteebc0WeC1qb08qmo1_500.jpg\",\"http://29.media.tumblr.com/tumblr_lixd8gn85W1qa1nfco1_500.jpg\",\"http://29.media.tumblr.com/tumblr_lsu0igEEtP1qiys5ao1_500.png\",\"http://28.media.tumblr.com/tumblr_lk8sapJ13n1qhb62wo1_500.jpg\",\"http://28.media.tumblr.com/tumblr_llr9e8mz5F1qaa50yo1_500.jpg\",\"http://30.media.tumblr.com/tumblr_liyjcg8NSv1qis1geo1_400.jpg\",\"http://30.media.tumblr.com/tumblr_lsx4nsg5Hj1qa25vco9_500.jpg\",\"http://25.media.tumblr.com/tumblr_lrvd8i2yMT1qd4jfno1_500.jpg\",\"http://27.media.tumblr.com/tumblr_liebn2Tx4Q1qb6gwio1_500.jpg\",\"http://25.media.tumblr.com/tumblr_mbs9uw4Uoy1qaa50yo1_500.jpg\",\"http://29.media.tumblr.com/tumblr_liezvwKrV71qaa50yo1_500.jpg\",\"http://27.media.tumblr.com/tumblr_lhwgc2NGyQ1qhl1obo1_500.jpg\",\"http://24.media.tumblr.com/tumblr_ll1h2iI9pG1qe76kxo1_500.jpg\",\"http://28.media.tumblr.com/tumblr_lk2i9iO0VS1qaa50yo1_500.jpg\",\"http://24.media.tumblr.com/tumblr_lk8ckdwc4c1qe3m7qo1_400.jpg\",\"http://28.media.tumblr.com/tumblr_lim8n49s881qa9dmvo1_500.jpg\",\"http://36.media.tumblr.com/tumblr_mc00rmFhJI1rh08hdo1_500.jpg\",\"http://25.media.tumblr.com/tumblr_mbnh52cctT1r3ip8io1_500.jpg\",\"http://29.media.tumblr.com/tumblr_ljuqnm49LZ1qzohdjo1_500.jpg\",\"http://28.media.tumblr.com/tumblr_ljlpq3J4e21qzgqodo1_500.jpg\",\"http://25.media.tumblr.com/tumblr_mcqpch4QbC1qb08qmo1_500.jpg\",\"http://25.media.tumblr.com/tumblr_ls9fa8S9lE1qfbmwho1_500.jpg\",\"http://24.media.tumblr.com/tumblr_lsm50jkqpy1qzhmgco1_500.jpg\",\"http://28.media.tumblr.com/tumblr_ljh4vnh0UZ1qb08qmo1_500.jpg\",\"http://27.media.tumblr.com/tumblr_liiy8xlcFx1qbeocxo1_500.jpg\",\"http://24.media.tumblr.com/tumblr_lhwsxaMJfm1qaa50yo1_500.jpg\",\"http://36.media.tumblr.com/tumblr_mblnsnvibw1r3pfomo1_500.jpg\",\"http://25.media.tumblr.com/tumblr_ltc8tzPvTT1qd5kcqo1_500.jpg\",\"http://24.media.tumblr.com/tumblr_mbg13ySRTZ1qbye9vo1_500.png\"]}",
//                PugResponse.class))
                .map(pugResponse -> {
                    //forward thinking change, so that way if services change to support a model not just raw strings (say names or descriptions, no difficult refactor required)
                    List<PugModel> pugs = new ArrayList<PugModel>(pugResponse.getPugs().size());
                    for (int i = 0; i < pugResponse.getPugs().size(); i++) {
                        pugs.add(new PugModel(pugResponse.getPugs().get(i)));
                    }
                    return pugs;
                })
                .subscribeOn(Schedulers.io())
                .compose(bindToLifecycle());

        obs.subscribe(pugs -> {
                    currentPugs.addAll(pugs);
                    pugStream.onNext(pugs);
                }
        );
        return obs;
    }

    public List<PugModel> getCurrentPugs() {
        return currentPugs;
    }

    public void onRestoredPugs(List<PugModel> pugModels) {
        currentPugs = pugModels;
    }
}
