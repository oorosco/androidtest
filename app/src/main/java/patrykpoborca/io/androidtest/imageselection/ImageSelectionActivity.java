package patrykpoborca.io.androidtest.imageselection;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;

import com.google.gson.reflect.TypeToken;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import patrykpoborca.io.androidtest.R;
import patrykpoborca.io.androidtest.base.BaseViewModelActivity;
import patrykpoborca.io.androidtest.imageselection.adapter.HorizontalPugAdapter;
import patrykpoborca.io.androidtest.imageselection.adapter.PugAdapter;
import patrykpoborca.io.androidtest.model.PugModel;
import rx.android.schedulers.AndroidSchedulers;

public class ImageSelectionActivity extends BaseViewModelActivity<ImageSelectionViewModel> implements PugAdapter.OnPugTouched {


    private static final String PUGS_KEY = "PUGS";
    @Bind(R.id.recycler)
    RecyclerView pugRecycler;
    private PugAdapter pugAdapter;
    @Bind(R.id.recycler_container)
    View container;
    @Bind(R.id.horizontal_recycler)
    RecyclerView horizontalRecycler;
    @Bind(R.id.dismiss)
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if(savedInstanceState != null) {
            getViewModel().onRestoredPugs(getGson().fromJson(savedInstanceState.getString(PUGS_KEY),
                    new TypeToken<List<PugModel>>(){}.getType()));
        }
    }

    @Override
    protected void setupPage() {
        super.setupPage();
        //region primary recycler
        pugAdapter = new PugAdapter(getViewModel().getPugStream().observeOn(AndroidSchedulers.mainThread()), getViewModel().getCurrentPugs(), this);
        pugRecycler.setAdapter(pugAdapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        pugRecycler.setLayoutManager(layoutManager);
        pugRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private int previousTotal = 0; // The total number of items in the dataset after the last load
            private int visibleThreshold = 2; // The minimum amount of items to have below your current scroll position before loading more.
            int firstVisibleItem, visibleItemCount, totalItemCount;
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = recyclerView.getChildCount()/3;
                totalItemCount = layoutManager.getItemCount()/3;
                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                if (!getViewModel().isLoading() && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    getViewModel().requestMorePugs()
                            .take(1)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(v -> previousTotal = totalItemCount);
                }
            }
        });
        //endregion

        view.setOnClickListener(v -> onBackPressed());
        //region secondary recycler

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        horizontalRecycler.setAdapter(new HorizontalPugAdapter(getViewModel().getPugStream().observeOn(AndroidSchedulers.mainThread()), getViewModel().getCurrentPugs(), this, width));
        horizontalRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        //endregion
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(PUGS_KEY, getGson().toJson(getViewModel().getCurrentPugs()));
    }

    @Override
    protected ImageSelectionViewModel initializeAndGetViewModel() {
        return new ImageSelectionViewModel();
    }

    @Override
    public void onPugTouched(int position) {
        container.setVisibility(View.VISIBLE);
        horizontalRecycler.scrollToPosition(position);
    }

    @Override
    public void onBackPressed() {
        if(container.getVisibility() == View.VISIBLE){
            container.setVisibility(View.GONE);
        }
        else {
            super.onBackPressed();
        }
    }
}
