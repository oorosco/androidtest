package patrykpoborca.io.androidtest.imageselection.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;
import java.util.concurrent.TimeUnit;

import patrykpoborca.io.androidtest.model.PugModel;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by patrykpoborca on 4/26/16.
 */
public class PugAdapter extends RecyclerView.Adapter<PugViewHolder>{

    private final List<PugModel> listOfPugs;
    private int visibleSize = 0;
    private OnPugTouched pugListener;
    private int currentSelection;

    public PugAdapter(Observable<List<PugModel>> pugStream, List<PugModel> currentPugs, OnPugTouched onPugTouched) {
        listOfPugs = currentPugs;
        pugListener = onPugTouched;
        pugStream.flatMap(Observable::from)
            .delay(50, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(pug -> {
                visibleSize ++;
                notifyItemInserted(visibleSize);
            });
    }

    @Override
    public PugViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return PugViewHolder.inflate(parent);
    }

    @Override
    public void onBindViewHolder(PugViewHolder holder, int position) {
        holder.onBind(listOfPugs.get(position));
        holder.pugImage.setOnClickListener(v -> {
            currentSelection = position;
            pugListener.onPugTouched(position);
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.setItemViewCacheSize(7);
    }


    @Override
    public int getItemCount() {
        return visibleSize;
    }

    public interface OnPugTouched{
        void onPugTouched(int position);
    }
}
