package patrykpoborca.io.androidtest.ui;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

/**
 * Created by patrykpoborca on 4/26/16.
 */
public class SquareContainer extends CardView {
    public SquareContainer(Context context) {
        super(context);
    }

    public SquareContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
