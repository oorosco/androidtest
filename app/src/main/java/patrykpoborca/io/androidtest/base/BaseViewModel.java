package patrykpoborca.io.androidtest.base;

import com.trello.rxlifecycle.ActivityEvent;
import com.trello.rxlifecycle.RxLifecycle;

import retrofit2.Retrofit;
import rx.Observable;

/**
 * Created by patrykpoborca on 4/26/16.
 */
public class BaseViewModel {

    private Observable<ActivityEvent> lifeCycleObservable;
    private Retrofit retrofit;

    public void onAttach() {

    }

    public void onDettach() {

    }

    void initializeViewModel(Observable<ActivityEvent> lifeCycleObservable, Retrofit retrofit){
        this.lifeCycleObservable = lifeCycleObservable;
        this.retrofit = retrofit;
    }

    protected final <T> Observable.Transformer<T, T> bindToLifecycle() {
        return RxLifecycle.bindActivity(lifeCycleObservable);
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
