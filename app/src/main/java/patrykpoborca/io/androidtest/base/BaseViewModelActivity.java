package patrykpoborca.io.androidtest.base;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.CallSuper;

import com.google.gson.Gson;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by patrykpoborca on 4/26/16.
 */
public abstract class BaseViewModelActivity<T extends BaseViewModel> extends RxAppCompatActivity {

    private Gson gson = new Gson();
    private T viewModel;
    private Handler handler = new Handler();
    private Retrofit retrofit;

    protected T getViewModel() {
        if (viewModel == null) {
            viewModel = initializeAndGetViewModel();
            viewModel.initializeViewModel(lifecycle(), getRetrofit());
        }
        return viewModel;
    }


    public Handler getHandler() {
        return handler;
    }

    protected void setupPage() {
    }

    public Gson getGson() {
        return gson;
    }

    protected abstract T initializeAndGetViewModel();

    public Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .baseUrl("https://pugme.herokuapp.com")
                    .build();
        }
        return retrofit;
    }

    //region lifecycle
    @CallSuper
    @Override
    protected void onStart() {
        super.onStart();
        setupPage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //inst vm
        getViewModel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getViewModel().onAttach();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getViewModel();
    }

    //endregion

}
