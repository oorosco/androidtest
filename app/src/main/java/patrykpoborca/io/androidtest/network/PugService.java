package patrykpoborca.io.androidtest.network;

import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by patrykpoborca on 4/26/16.
 */
public interface PugService {

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET("bomb")
    Observable<PugResponse> getPugs(@Query("count") int itemCount);

    class Factory{
        private Factory(){}

        public static PugService instantiate(Retrofit retrofit) {
            return retrofit.create(PugService.class);
        }
    }

}
