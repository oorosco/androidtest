package patrykpoborca.io.androidtest.network;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by patrykpoborca on 4/26/16.
 */
public class PugResponse {

    @SerializedName("pugs")
    private List<String> pugs;

    public List<String> getPugs() {
        return pugs;
    }
}
