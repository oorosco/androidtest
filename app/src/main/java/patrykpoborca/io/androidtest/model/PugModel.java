package patrykpoborca.io.androidtest.model;

/**
 * Created by patrykpoborca on 4/26/16.
 */
public class PugModel {

    public PugModel(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
